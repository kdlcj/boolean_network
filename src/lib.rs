

use std::cell::RefCell;
use bdd::Bdd;



#[derive(Debug, PartialEq, Clone)]
pub enum Regulation {
    INHIBITION, ACTIVATION //, UNSPECIFIED
}



#[derive(Debug, Clone)]
pub enum BehavioralClass {
    STABILITY, OSCILLATION, DISORDER
}



#[derive(Debug, Clone)]
struct BooleanNetworkEdge {
    kind: Regulation,
    to: usize,
    observable: bool
}



/// Represents a Boolean Network as a directed graph, where edge weights
/// are a members of _{inhibition, activation} × {observable, not observable}_.
/// Each graph vertex is assigned a named variable.
#[derive(Debug, Clone)]
pub struct BooleanNetwork {
    graph: Vec<Vec<BooleanNetworkEdge>>,
    variables: Vec<String>,
    parametrizations_cache: RefCell<Option<Bdd>>
    // constraints: Vec<(String, Bdd)>
}

impl BooleanNetwork {
    pub fn new() -> Self {
        BooleanNetwork {
            graph: Vec::new(),
            variables: Vec::new(),
            parametrizations_cache: RefCell::new(None)
        }
    }

    /// Returns a new BooleanNetwork from the SBML string
    pub fn from_sbml(sbml_string: &str) -> Result<Self, String> {
        use quick_xml::Reader;
        use quick_xml::events::Event;
        use std::collections::HashMap;

        let mut boolnet = Self::new();
        let mut reader = Reader::from_str(sbml_string);
        let mut buf = Vec::new();
        let mut id_to_name: HashMap<String, String> = HashMap::new();
        let tostr = |x: &[u8]| -> String { String::from(std::str::from_utf8(x).unwrap()) };

        let mut regulators = Vec::new();

        loop { match reader.read_event(&mut buf) {
            Err(event) => return Err(format!("SMBL syntax error at {}: {:?}", reader.buffer_position(), event)),
            Ok(Event::Eof) => break,
            Ok(Event::Start(event)) => match event.name() {
                b"qual:qualitativeSpecies" => {
                    let mut pair = (None, None);
                    for attribute in event.attributes() {
                        let attribute = attribute.unwrap();
                        match attribute.key {
                            b"qual:id" => pair.0 = Some(tostr(&attribute.value)),
                            b"qual:name" => pair.1 = Some(tostr(&attribute.value)),
                            _ => ()
                        }
                    }
                    id_to_name.insert(pair.0.unwrap(), pair.1.unwrap());
                },
                b"qual:input" => {
                    let mut pair = (None, None);
                    for attribute in event.attributes() {
                        let attribute = attribute.unwrap();
                        match attribute.key {
                            b"qual:qualitativeSpecies" => pair.0 = Some(tostr(&attribute.value)),
                            b"qual:sign"               => pair.1 = Some(tostr(&attribute.value)),
                            _ => ()
                        }
                    }
                    regulators.push((pair.0.unwrap(), pair.1.unwrap()));
                },
                _ => ()
            },
            Ok(Event::Empty(event)) => match event.name() {
                b"qual:output" => for attribute in event.attributes() {
                    let attribute = attribute.unwrap();
                    match attribute.key {
                        b"qual:qualitativeSpecies" => for (regulator, sign) in &regulators {
                            let kind = if sign == "positive" { Regulation::ACTIVATION }
                                       else                  { Regulation::INHIBITION };
                            boolnet.add_edge(kind, id_to_name.get(regulator).unwrap(),
                                id_to_name.get(&tostr(&attribute.value)).unwrap(), true);
                        },
                        _ => ()
                    }
                },
                _ => ()
            },
            Ok(Event::End(event)) => match event.name() {
                b"qual:transition" => regulators.clear(),
                _ => ()
            },
            _ => ()
        }}

        if (boolnet.len() == 0) {
            return Err(String::from("Model is not in the SBML format."))
        }

        Ok(boolnet)
    }

    /// Reverses the direction of arrows. This is useful for
    /// fetching regulators (instead of successors) in constant time.
    /// Order of variables is identical to that of the original network.
    pub fn inverse(&self) -> Self {
        let mut graph = vec![Vec::new(); self.graph.len()];
        for (from, succ_list) in self.graph.iter().enumerate() {
            for successor in succ_list {
                let new_successor = BooleanNetworkEdge { 
                    to: from,
                    observable: successor.observable,
                    kind: successor.kind.clone()
                };
                graph[successor.to].push(new_successor)
            }
        }

        for succ_list in &mut graph {
            succ_list.sort_unstable_by(|a, b| a.to.cmp(&b.to))
        }

        BooleanNetwork {
            graph,
            variables: self.variables.clone(),
            parametrizations_cache: RefCell::new(None)
        }
    }

    pub fn add_activation(&mut self, from: &str, to: &str, observable: bool) -> &mut Self {
        self.add_edge(Regulation::ACTIVATION, from, to, observable);

        self
    }

    pub fn add_inhibition(&mut self, from: &str, to: &str, observable: bool) -> &mut Self {
        self.add_edge(Regulation::INHIBITION, from, to, observable);

        self
    }

    pub fn state_space(&self) -> StateSpace {
        StateSpace::from(self)
    }

    fn discard_cache(&mut self) {
        self.parametrizations_cache.replace(None);
    }

    fn add_edge(&mut self, kind: Regulation, from: &str, to: &str, observable: bool) {
        let mut indices = [0, 0];
        let from_to = [from, to];
        self.discard_cache();

        for (iteration, new_index) in indices.iter_mut().enumerate() {
            *new_index = match self.var_index(from_to[iteration]) {
                Some(x) => x,
                None => {
                    self.graph.push(Vec::new());
                    self.variables.push(String::from(from_to[iteration]));

                    self.graph.len() - 1
                }
            };
        }
        let new_edge = BooleanNetworkEdge { to: indices[1], observable, kind };
        self.graph[indices[0]].push(new_edge);
        self.graph[indices[0]].sort_unstable_by(|a, b| a.to.cmp(&b.to));
    }

    /// Returns the number of parameters the Boolean Network has.
    pub fn param_len(&self) -> u32 {
        self.inverse().graph.iter().map(|vec| 2u32.pow(vec.len() as u32)).fold(0, |a, x| a + x)
    }

    /// Returns name of a variable, located at the given index.
    pub fn var_name(&self, index: usize) -> Option<&String> {
        if index < self.len() {
            return Some(&self.variables[index])
        }

        None
    }

    /// Transforms name of a parameter to its index. Useful
    /// for BDDs, which are storing only numbers as variables.
    /// Example: "p$m2n$0" -> Some(0)
    pub fn parameter_to_index(&self, name: &str) -> Option<usize> {
        let segments = name.split('$').collect::<Vec<_>>();
        let mut offset = 0;
        for (index, succlist) in self.inverse().graph.iter().enumerate() {
            if self.var_name(index)? == segments.get(1)? {
                return Some(offset + segments[2].parse::<usize>().unwrap())
            }
            offset += 2u32.pow(succlist.len() as u32) as usize
        }

        None
    }

    /// Transforms an index of a parameter into it's full name.
    /// Example: 2 -> Some("p$m2n$3")
    pub fn index_to_parameter(&self, mut var_index: usize) -> Option<String> {
        for (index, succlist) in self.inverse().graph.iter().enumerate() {
            let power_of_two = 2u32.pow(succlist.len() as u32) as usize;
            if var_index < power_of_two {
                return Some(format!("p${}${}", self.var_name(index)?, var_index))
            }
            var_index -= power_of_two
        }

        None
    }

    /// Returns an ordered list of names of parameters
    /// of the Boolean Network.
    pub fn all_params(&self) -> Vec<String> {
        let mut result = Vec::new();
        for i in 0..self.param_len() {
            result.push(self.index_to_parameter(i as usize).unwrap())
        }

        result
    }

    /// Converts a variable name into an index, as
    /// stored in the Boolean Network.
    pub fn var_index(&self, name: &str) -> Option<usize> {
        for (index, variable) in self.variables.iter().enumerate() {
            if variable == name {
                return Some(index)
            }
        }

        None
    }

    /// Returns the boolean network as a string in the DOT format for visualization.
    pub fn to_dot(&self) -> String {
        use self::Regulation::*;

        let mut result = String::from("digraph d { layout=circo\n");
        for (index, successors) in self.graph.iter().enumerate() {
            let from_name = &self.variables[index];
            for BooleanNetworkEdge { to, kind, observable } in successors {
                let (color, arrow) = if *kind == ACTIVATION {
                    ("green", "normal") } else { ("red", "tee") };
                result += &format!("{} -> {} [color={} arrowhead={} label=\"{}\"]\n",
                    *from_name, self.var_name(*to).unwrap(),
                    color, arrow, if *observable { "" } else { "?" });
            }
        }

        result + "}"
    }

    pub fn to_sbml(&self) -> String {
        unimplemented!()
    }

    pub fn len(&self) -> usize {
        self.variables.len()
    }

    /// Generates BDD of valid parametrizations. Order of parameters
    /// is given by the `variables` attribute.
    pub fn parametrizations(&self) -> Bdd {
        use self::Regulation::*;
        let pot = |x| 2u32.pow(x as u32);

        if let Some(params) = self.parametrizations_cache.borrow().as_ref() {
            return params.clone()
        }

        let mut final_bdd  = Bdd::terminal(true);
        for (index, regs) in self.inverse().graph.iter().enumerate() {
            let mut or_bdd = Bdd::terminal(false);
            for (column, edge) in regs.iter().enumerate() {
                let offset = pot(column); 
                let mut origin_row = 0;
                for row in 1..=pot(regs.len()) / 2 {
                    let p1 = Bdd::variable(self.parameter_to_index(&format!("p${}${}", self.variables[index], origin_row)).unwrap());
                    let p2 = Bdd::variable(self.parameter_to_index(&format!("p${}${}", self.variables[index], origin_row + offset)).unwrap());
                    final_bdd = match edge.kind {
                        ACTIVATION => &final_bdd & &Bdd::implies(&p1, &p2),
                        INHIBITION => &final_bdd & &Bdd::implies(&p2, &p1)
                    };

                    if edge.observable {
                        or_bdd = &or_bdd | &Bdd::xor(&p1, &p2);
                    }

                    origin_row += 1;
                    if row % offset == 0 {
                        origin_row += offset
                    }
                }
                if Bdd::sat(&or_bdd) {
                    final_bdd = &final_bdd & &or_bdd;
                }
            }
        }

        self.parametrizations_cache.replace(Some(final_bdd.clone()));
        final_bdd
    }
}

/// Enables loading of a Boolean Network from string, maybe useful
/// for loading from a file. The current format syntax is a subject to change.
/// Fragment of the `Boolsim` format.
impl std::str::FromStr for BooleanNetwork {
    type Err = String;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        use Regulation::*;
        let mut boolnet = Self::new();
        for (n, line) in string.lines().enumerate() {
            let f_k_t = line.split_whitespace().collect::<Vec<_>>();
            match f_k_t.len() {
                0 => continue,
                3 => (),
                _ => return Err(format!("Invalid line '{}' on line {}.", line, n + 1))
            }
            let (kind, observable) = match f_k_t[1] {
                "->"  => (ACTIVATION, true),
                "->?" => (ACTIVATION, false),
                "-|"  => (INHIBITION, true),
                "-|?" => (INHIBITION, false),
                _     => return Err(format!("Invalid arrow '{}' on line {}.", f_k_t[1], n + 1))
            };
            boolnet.add_edge(kind, f_k_t[0], f_k_t[2], observable);
        }

        Ok(boolnet)
    }
}

/// Output of the fmt method has the same form as the expected input of the from_str method.
impl std::fmt::Display for BooleanNetwork {
    fn fmt(&self, formatter: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use Regulation::*;

        for (from, succ_list) in self.graph.iter().enumerate() {
            for edge in succ_list {
                let (k, o) = match (&edge.kind, edge.observable) {
                    (ACTIVATION, true)  => ("->", ""),
                    (INHIBITION, true)  => ("-|", ""),
                    (ACTIVATION, false) => ("->", "?"),
                    (INHIBITION, false) => ("-|", "?")
                };
                write!(formatter, "{} {}{} {}\n",
                    self.variables[from], k, o,
                    self.variables[edge.to])?;
            }
        }
        
        Ok(())
    }
}



/// Result of the terminal components' algorithm
/// is a mapping from a set of parametrizations
/// to a multiset of behaviors.
#[derive(Debug, Clone)]
pub struct ComponentsResult {
    result: Vec<(Vec<BehavioralClass>, Bdd)>,
}

impl ComponentsResult {
    pub fn new() -> Self {
        ComponentsResult { result: Vec::new() }
    }

    pub fn insert(&mut self, class: &BehavioralClass, parametrization: &Bdd) {
        for (pheno, bdd) in &self.result {
            println!("unimplemented")
        }
    }

    pub fn len(&self) -> usize {
        self.result.len()
    }
    
    pub fn result_vector(&self) -> &Vec<(Vec<BehavioralClass>, Bdd)> {
        &self.result
    }
}

pub fn _test_result() -> ComponentsResult {
    use BehavioralClass::*;
    ComponentsResult { result: vec![
        (vec![STABILITY, OSCILLATION], Bdd::from_table(&vec!["1001", "1010", "1011"])),
        (vec![STABILITY],              Bdd::from_table(&vec!["11111", "10011", "10000"])),
        (vec![DISORDER, STABILITY],    Bdd::from_table(&vec!["11", "10"]))] }
}

#[derive(Debug, Clone)]
struct StateSpaceEdge {
    to: usize,
    under_params: Bdd
}



/// Represents a directed graph of states — valuations of variables. 
/// Edge weights are BDDs, representing parametrizations, for which
/// the edge is present.
#[derive(Debug)]
pub struct StateSpace {
    graph: Vec<Vec<StateSpaceEdge>>,
    boolnet: BooleanNetwork
    // attractors:
}

impl StateSpace {
    pub fn new() -> Self {
        StateSpace {
            graph: Vec::new(),
            boolnet: BooleanNetwork::new()
        }
    }

    /// Constructs a new State Space from a given Boolean Network
    /// with asynchronous semantics.
    pub fn from(boolnet: &BooleanNetwork) -> Self {
        let pot    = |x| 2u32.pow(x as u32) as usize;
        let toggle = |n, k: usize| n ^ (1 << k);
        let bit    = |n: usize, k: usize| (n & (1 << k) != 0) as usize;

        let inverse = boolnet.inverse();
        let parametrizations = boolnet.parametrizations();
        let mut graph = vec![Vec::new(); pot(boolnet.len())];

        for (from_index, succ_list) in graph.iter_mut().enumerate() {
            for var_pointer in 0..inverse.len() {
                let mut parameter_index = 0;
                for (i, edge) in inverse.graph[var_pointer].iter().enumerate() {
                    parameter_index |= bit(from_index, edge.to) << i;
                }

                let new_edge = StateSpaceEdge {
                    to: toggle(from_index, var_pointer),
                    under_params: {
                        let t1 = Bdd::terminal(bit(toggle(from_index, var_pointer), var_pointer) != 0);
                        let t2 = Bdd::variable(boolnet.parameter_to_index(&format!("p${}${}", inverse.variables[var_pointer], parameter_index)).unwrap());
                        &parametrizations & &Bdd::iff(&t1, &t2)
                    }
                };
                succ_list.push(new_edge);
            }
        }

        StateSpace { graph, boolnet: boolnet.clone() }
    }

    /// Inverse State Space. Order of states is left unchanged.
    pub fn inverse(&self) -> Self {
        Self::from(&self.boolnet.inverse())
    }

    /// Returns vector of variables, as ordered in states. The order
    /// of variables is identical to that of the underlying Boolean Network.
    pub fn vars(&self) -> &Vec<String> {
        &self.boolnet.variables
    }

    /// Returns a number of variables. It always holds that `vars_len` = _log_ `len`.
    pub fn vars_len(&self) -> usize {
        self.boolnet.len()
    }

    /// Return a number of states in the State Space.
    pub fn len(&self) -> usize {
        self.graph.len()
    }

    /// Compute immediate parametrized predecessors of a set of states.
    /// pre(S)(s) = { p ∈ P | ∃t ∈ Π(B) : p ∈ S(t) ∩ C(s, t) }
    pub fn pre(&self, param_state_set: &Vec<Bdd>) -> Vec<Bdd> {
        self.step(param_state_set, true)
    }

    /// Compute immediate parametrized successors a set of states.
    /// post(S)(s) = { p ∈ P | ∃t ∈ Π(B) : p ∈ S(t) ∩ C(t, s) }
    pub fn post(&self, param_state_set: &Vec<Bdd>) -> Vec<Bdd> {
        self.step(param_state_set, false)
    }

    fn step(&self, param_state_set: &Vec<Bdd>, pre: bool) -> Vec<Bdd> {
        assert!(self.len() == param_state_set.len());

        let inverse = self.inverse().graph;
        let graph = if pre { &self.graph } else { &inverse };
        let mut result = Vec::new();

        for succ_list in graph {
            let mut state_bdd = Bdd::terminal(false);
            for succ in succ_list {
                state_bdd = &state_bdd | &(&param_state_set[succ.to] | &succ.under_params);
            }
            result.push(state_bdd);
        }

        result
    }

    /// Closure of the `pre` operator.
    /// bwd(C, S) = S ∪ (C ∩ pre(bwd(C, D)))
    pub fn bwd(&self, set_c: &Vec<Bdd>, set_s: &Vec<Bdd>) -> Vec<Bdd> {
        self.step_closure(set_c, set_s, &Self::pre)
    }

    /// Closure of the `post` operator.
    /// fwd(C, S) = S ∪ (C ∩ post(fwd(C, D)))
    pub fn fwd(&self, set_c: &Vec<Bdd>, set_s: &Vec<Bdd>) -> Vec<Bdd> {
        self.step_closure(set_c, set_s, &Self::post)
    }

    fn step_closure(&self, set_c: &Vec<Bdd>, set_s: &Vec<Bdd>, fun: &dyn Fn(&Self, &Vec<Bdd>) -> Vec<Bdd>) -> Vec<Bdd> {
        let mut before = set_s.clone();
        let mut acc = Bdd::zip(&Bdd::or, &set_s, &Bdd::zip(&Bdd::and, &fun(self, &before), &set_c));
        while !acc.iter().zip(before.iter()).map(|(a, b)| a == b).fold(true, |a, x| a && x) { // make this short circuit !!
            before = acc;
            acc = Bdd::zip(&Bdd::or, &set_s, &Bdd::zip(&Bdd::and, &fun(self, &before), &set_c));
        }

        acc
    }

    /// nonempty_params(S) == P(S) to denote all
    /// parametrizations for which S contains some states
    /// i.e. P(S) = {p ∈ P | ∃s ∈ Π(B) : p ∈ S(s)}.
    pub fn nonempty_params(&self, set_s: &Vec<Bdd>) -> Bdd {
        assert!(self.len() == set_s.len());

        let mut result = Bdd::terminal(false);
        for (state_index, _) in self.graph.iter().enumerate() {
            result = &result | &set_s[state_index];
        }

        &self.boolnet.parametrizations() & &result
    }

    /// Pivots will select one representant from S for every
    /// parametrization in P(S): for every p ∈ P(S) there is
    /// exactly one s for which p ∈ result(s).
    pub fn pivots(&self, set: &Vec<Bdd>) -> Vec<Bdd> {
        let parametrizations = self.boolnet.parametrizations();
        let mut result = set.clone();

        for mut bdd in &result { // !!
            bdd = &Bdd::and(&bdd, &parametrizations);
        }

        for (from, bdd) in set.iter().enumerate() {
            for to in from + 1..set.len() {
                result[to] = Bdd::and_not(&result[to], &bdd);
            }
        }

        result
    }

    pub fn classify(&self, set_a: Vec<Bdd>, result: &mut ComponentsResult) {
        use BehavioralClass::*;
        
        let vec_sat = |v: &Vec<_>| { for bdd in v { if Bdd::sat(&bdd) { return true } } false };
        let parametrizations = self.boolnet.parametrizations();
        let mut iteration_set_a = vec![self.pivots(&set_a)];
        let mut iteration_set_i = vec![Bdd::terminal(false); self.vars_len()];
        let mut d = Bdd::terminal(false);
        let mut k = 0;
        let mut set_e = self.post(&iteration_set_a[0]);

        while vec_sat(&set_e) {
            for i in 0..k {
                iteration_set_i[i] = self.nonempty_params(&Bdd::zip(&Bdd::and, &iteration_set_a[i], &set_e));
            }

            let mut pairwise_intersections = Vec::new(); Bdd::terminal(true);
            for i in 0..=k {
                for j in 0..=k {
                    if j == i { continue; }
                    pairwise_intersections.push(&iteration_set_i[j] & &iteration_set_i[j]);
                }
            }
            let mut sum = Bdd::terminal(false);
            for bdd in pairwise_intersections {
                sum = &sum | &bdd;
            }
            d = &d | &sum;

            let mut sum = Bdd::terminal(false);
            for i in 0..=k {
                sum = &sum | &iteration_set_i[i];
            }
            iteration_set_i[k + 1] = Bdd::and_not(&self.nonempty_params(&set_e), &sum);

            if Bdd::sat(&iteration_set_i[k + 1]) {
                k += 1;
            }

            let mut sum = vec![Bdd::terminal(false); iteration_set_a[0].len()];
            for i in 0..k {
                sum = Bdd::zip(&Bdd::or, &sum, &iteration_set_a[i]);
            }
            set_e = Bdd::zip(&Bdd::and_not, &Bdd::restrict(&set_e, &Bdd::and_not(&parametrizations, &d)), &sum);

            for i in 0..k {
                iteration_set_a[i] = Bdd::zip(&Bdd::or, &iteration_set_a[i], &Bdd::restrict(&set_e, &iteration_set_i[i]));
            }

            set_e = self.post(&set_e);
        }

        self.discovered(DISORDER, &d, result);
        self.discovered(OSCILLATION, &Bdd::and_not(&self.nonempty_params(&set_a), &d), result);
    }

    pub fn discovered(&self, class: BehavioralClass, parametrization: &Bdd, result: &mut ComponentsResult) {
        println!("discovered {:?} {}", class, parametrization.to_dot_with_names(&self.boolnet.all_params()));
        result.insert(&class, &parametrization);
    }

    /// Implementation of the Terminal Strongly-Connected Components algorithm from the paper.
    pub fn tcss(&self, set_v: Vec<Bdd>, result: &mut ComponentsResult) {
        let p = |set| self.nonempty_params(set);

        if !Bdd::sat(&p(&set_v)) { return }

        let set_p = self.pivots(&set_v);
        let set_f = self.fwd(&set_v, &set_p);
        let set_b = self.bwd(&set_f, &set_p);

        let t = Bdd::and_not(&p(&set_b), &p(&Bdd::zip(&Bdd::and_not, &set_f, &set_b)));

        self.classify(Bdd::restrict(&set_b, &t), result);
        self.tcss(Bdd::zip(&Bdd::and_not, &set_f, &set_b), result);
        self.tcss(Bdd::zip(&Bdd::and_not, &set_v, &self.bwd(&set_v, &set_f)), result);
    }

    pub fn terminal_components(&self) -> ComponentsResult {
        let params = self.boolnet.parametrizations();
        let mut result = ComponentsResult::new();
        self.tcss(vec![params; self.len()], &mut result);

        _test_result()
        //result
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn and() {
        use bdd::Bdd;
        let bdd_false = Bdd::terminal(false);
        let bdd_true = Bdd::terminal(true);

        // 1 = 1 & 1
        assert!(Bdd::eq(&bdd_true, &Bdd::and(&bdd_true, &bdd_true)));
        // 1 = 1 & !0
        assert!(Bdd::eq(&bdd_true, &Bdd::and(&bdd_true, &Bdd::not(&bdd_false))));
        // 1 != 0
        assert!(!Bdd::eq(&bdd_true, &bdd_false));
        // 0 = 1 & 0
        assert!(Bdd::eq(&bdd_false, &Bdd::and(&bdd_true, &bdd_false)));
    }

    #[test]
    fn more_expressions() {
        use std::str::FromStr;
        let bdd = BooleanNetwork::from_str(
            "a -> d
             f -> g
             h -|? h
             a -| f
             d -> a"
        ).unwrap().parametrizations();

        // A & !(A & A) = 0
        let same = Bdd::and(&bdd, &bdd);
        let negation = Bdd::not(&same);
        let unsat = Bdd::and(&negation, &bdd);

        assert!(!Bdd::sat(&unsat));
        assert!(Bdd::eq(&Bdd::terminal(false), &unsat));
    }

    #[test]
    fn parametrizations() {
        use std::str::FromStr;
        use std::time::Instant;
        let paper_example = BooleanNetwork::from_str(
           "m2n -| p53
            dna ->? dna
            m2c -> m2n
            p53 -> m2c
            p53 -| dna
            p53 -| m2n
            dna -| m2n"
        ).unwrap();
        let parametrizations = paper_example.parametrizations();
        let inverse = paper_example.inverse();
        let valids = vec![("m2n", vec![0b00001000, 0b00001110, 0b10001010, 0b10001100,
                                       0b10001110, 0b10001111, 0b10101110, 0b11001110, 0b11101111]),
                          ("dna", vec![0b0010, 0b1010, 0b1011]),
                          ("m2c", vec![0b01]),
                          ("p53", vec![0b10])];

        for _ in 0..100 {
            let random = Instant::now().elapsed().subsec_nanos();
            let mut map = vec![false; paper_example.param_len() as usize];
            for (name, valid_columns) in &valids {
                let col = valid_columns[(random % valid_columns.len() as u32) as usize];
                let index = inverse.var_index(name).unwrap();
                let number_of_rows = 2u32.pow(inverse.graph[index].len() as u32);
                for (x, i) in (0..number_of_rows).rev().enumerate() {
                    let parameter = format!("P${}${}", name, x);
                    let value = col & (1 << i) != 0;
                    map[paper_example.parameter_to_index(&parameter).unwrap()] = value;
                }
            }
            assert!(Bdd::eval(&parametrizations, &map));
        }
    }

    #[test]
    fn pivots() {
        let a = "0000";
        let b = "0001";
        let c = "0010";
        let d = "0011";
        let e = "0100";
        let f = "0101";
        let g = "0110";
        let h = "0111";
    
        let first  = Bdd::from_table(&vec![a, b, c, d]);
        let second = Bdd::from_table(&vec![a, b, e]);
        let third  = Bdd::from_table(&vec![a, g, h]);
        let fourth = Bdd::from_table(&vec![a, c]);
       
        let vector = vec![first, second, third, fourth];

        assert!(false)
    } 

    #[test]
    fn terminal_components() {
        use std::str::FromStr;

        let boolnet = BooleanNetwork::from_str("a -> b\nc ->? d").unwrap();
        boolnet.state_space().terminal_components();
    }

    #[test]
    fn implication_has_6_valid_assignments() {
        use std::str::FromStr;

        let boolnet = BooleanNetwork::from_str("a ->? b").unwrap();

        // six because of the free parameter of function F_a
        assert!(boolnet.parametrizations().sat_count() == 6)
    }

    #[test]
    fn nonempty_params() {
        assert!(false)
    }
}
